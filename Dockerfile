FROM golang:1.13-alpine as build
RUN mkdir /app
ADD . /app/
WORKDIR /app
COPY main.go .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /app .
EXPOSE 8080
CMD ["./main"]  
